from sympy import symbols, cos, pi
from sympy.core.numbers import One
from sympy.utilities.lambdify import lambdify
from math import sin, pi
import numpy as np

import matplotlib.pyplot as plt

def relu_func(x):
   return np.maximum(0,x)
    

def chebyshev_approx(function, intr, deg=5, prec=15):
    n = deg + 1

    x, u = symbols('x u')

    a, b = intr
    x_to_u = (2 * x - a - b) / (b - a)
    u_to_x = (b - a) / 2 * u + (a + b) / 2
    chebyshev_nodes = cos((symbols('i') + 0.5) / n * pi)

    result_u = [ chebyshev_nodes.evalf(prec, subs={'i': i}) for i in range(n) ]
    result_x = [ u_to_x.evalf(prec, subs={u: i}) for i in result_u ]
    result_y = [ function(i) for i in result_x ]

    t = [One(), u]

    for _ in range(n-2):
        t.append(2 * u * t[-1] - t[-2])

    c = [ sum(result_y) / n ]

    for index in range(1, n):
        c.append( 2 * sum(t[index].evalf(prec, subs={u: i}) * j for i, j in zip(result_u, result_y)) / n )

    y = 1 * c[0]

    for i in range(1, n):
        y += t[i] * c[i]

    y = y.subs({u: x_to_u}).simplify()

    f = lambdify(x, y)
    
    f.formula = y

    return f


f = chebyshev_approx(leaky_relu, (-10, 10), 5)


x = np.linspace(-10, 10, 100) 

# For Relu
z=np.maximum(0,x)
# y=2.368475785867e-19*x**5 - 0.000252624921308674*x**4 - 2.90138283768708e-17*x**3 + 0.0660873211772537*x**2 + 0.500000000000001*x + 0.862730150341736


# y=2.30556314780491e-19*x**5 - 0.000250098672095587*x**4 - 2.83384427035571e-17*x**3 + 0.0654264479654812*x**2 + 0.505000000000001*x + 0.854102848838318
y=f(x)
print(y)
# y=str(f.formula)
# y=eval(y)
# print(y)
# print('y',y.shape)  
# a= np.float(-8.606e−10)*x^3+ (1.330e−17)*x^2+ 0.001*x+ 0.499

# prepare the plot, associate the color r(ed) or b(lue) and the label 
plt.plot(x, z, 'r', label='ReLU')
plt.plot(x, y, 'b', label='Approximation')
# plt.plot(x, a, 'o', label='Previous')

plt.grid()

plt.title('ReLU Function')


plt.legend(loc='lower right')

# plt.plot(x)
plt.xlabel('X Axis')
plt.ylabel('Y Axis')

# create the graph
plt.savefig("Relu_3_10.png")
plt.show()
