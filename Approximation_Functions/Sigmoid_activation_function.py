from sympy import symbols, cos, pi
from sympy.core.numbers import One
from sympy.utilities.lambdify import lambdify
from math import sin, pi
import matplotlib.pyplot as plt
import numpy as np

import numpy as np


def sig_func(x):
    x=np.float(x)
    y=1/(1 + np.exp(-x))
    # return y*(1-y)
    return y


def chebyshev_approx(function, intr, deg=5, prec=15):
    n = deg + 1

    x, u = symbols('x u')

    a, b = intr
    x_to_u = (2 * x - a - b) / (b - a)
    u_to_x = (b - a) / 2 * u + (a + b) / 2
    chebyshev_nodes = cos((symbols('i') + 0.5) / n * pi)

    result_u = [chebyshev_nodes.evalf(prec, subs={'i': i}) for i in range(n)]
    result_x = [u_to_x.evalf(prec, subs={u: i}) for i in result_u]
    result_y = [function(i) for i in result_x]

    t = [One(), u]

    for _ in range(n - 2):
        t.append(2 * u * t[-1] - t[-2])

    c = [sum(result_y) / n]

    for index in range(1, n):
        c.append(2 * sum(t[index].evalf(prec, subs={u: i}) * j for i, j in zip(result_u, result_y)) / n)

    y = 1 * c[0]

    for i in range(1, n):
        y += t[i] * c[i]

    y = y.subs({u: x_to_u}).simplify()

    f = lambdify(x, y)

    f.formula = y

    return f


f = chebyshev_approx(sig_func, (-10, 10), 5)


x = np.linspace(-10, 10, 100)
# For Sigmoid function
z = 1/(1 + np.exp(-x))

y=f(x)
print(y)
plt.plot(x, z, 'r', label='Sigmoid')
plt.plot(x, y, 'b', label='Approximation')

plt.grid()

plt.title('Sigmoid Function')


plt.legend(loc='lower right')

# plt.plot(x)
plt.xlabel('X Axis')
plt.ylabel('Y Axis')

# create the graph
plt.savefig("Sigmoid_3_10.png")
plt.show()
