#      Blind Faith:Privacy-Preserving Machine Learning
#            using Function Approximation

In this work, we propose Blind Faith, a machine learning model in which the training phase takes place in plaintext data, while the classification of user inputs takes place on homomorphically encrypted ciphertexts. To make our model compatible with homomorphic encryption, we use Chebyshev polynomials to approximate the activation functions. This enables us to create a machine learning model that can classify encrypted images while maintaining privacy.

## Description
In this work, we proposed a privacy preserving machine learning model in which the CNN can be applied directly on encrypted data. We preserve the privacy of the users by utilizing the functionalities offered by HE. To make the CNN model compatible with HE, we show how to approximate the ReLU and Sigmoid activation functions, using low degree Chebyshev polynomials.

## Organization
The project consist of the following files:

### Approximation_Functions

We approximate the following activation functions:
1. ReLU: The polynomial approximation of the ReLU activation function is given in Relu_activation_function.py
2. Sigmoid: The polynomial approximation of the Sigmoid activation function is given in Sigmoid_activation_function.py


### Approx_Model

First, we train the CNN model using the ReLU activation function. Then we train the same model, however this time the activation function is replaced with its polynomial approximation. Finally, we compare the models using accuracy as an evaluation metric. 

### homomorphic_Encryption

### Library Files

The rest of the folder consist of Simple Encrypted Arithmetic Library (SEAL) library files as well as a Python wrapper to SEAL.

### 

### Note: 
The implementation of some of the algorithm in this work is borrowed from different projects such as for
- Chebyshev approximation: for some part we rely on the work given in [3].
- For SEAL and PySEAL Wrapper [1] and [5] are taken into account.
- For Homomorphic encryption we depend on the code given in [2]. 
However the implementation have been developed as part of this project.

## References
[1] https://github.com/Lab41/PySEAL

[2] https://github.com/shreyagarge/EncryptedImagePredictorNetwork

[3] https://github.com/wdv4758h/chebyshev/blob/master/chebyshev_sympy.py

[4] https://github.com/pychebfun/pychebfun

[5] http://sealcrypto.org/
