import numpy as np 
from Predictor.utils import TimerContext
from Predictor.logger import Logger
# give path relative to docker root (Encrypted_NN)

path = {'weights':'Predictor/server_net/models/model/weights/',
        'biases':'Predictor/server_net/models/model/biases/'}


class Predictor:

    def __init__(self, debug = False, handler = None):

        self._encoder = handler.encoder
        self._encryptor = handler.encryptor
        self._evaluator = handler.evaluator
        self._Ciphertext = handler.Ciphertext
        self._ev_key = handler.evaluation_key
        self._denoise = handler.re_encrypt
        self._logger = Logger(debug=debug, handler=handler)
        self._model = self._load_model()

    def _load_model(self):
        _mapping = {
            'wc1': "{}weights1.npy".format(path['weights']),
            'wc2': "{}weights2.npy".format(path['weights']),
            'wf1': "{}weights3.npy".format(path['weights']),
            'otw': "{}weights4.npy".format(path['weights']),
            'bc1': "{}bc1.npy".format(path['biases']),
            'bc2': "{}bc2.npy".format(path['biases']),
            'bf1': "{}bd1.npy".format(path['biases']),
            'otb': "{}bout.npy".format(path['biases']),
        }
        
        model = {}
        for key, value in _mapping.items():
            model[key] = np.load(value)

        return model

    def predict_image(self, image):

        # image is 724 long Ciphertext object array
        self._logger._log_message('Reshaping Image to 1 x 28 x 28 x 1')
        image = np.reshape(image,(1,28,28,1))

        # layer 1 - convolution, bias addition 
        with TimerContext(headline='First convolution', message="time taken for first convolution:"):
            con = self._conv2d(image,self._model['wc1'])
            con = self._add_bias(con,self._model['bc1'])

        # self._logger._log_message('con1b shape: %s', str(con.shape))
        self._logger._log_noise_budget(message="Noise budget after first convolution", value=con[0,15,15,1])

        # layer 2 - activation 
        with TimerContext(headline='activation', message="time taken for first activation function:"):
            con = self._activ_fun(con)
    
        # self._logger._log_message('con1af shape: %s', str(con.shape))
        self._logger._log_noise_budget(message="Noise budget after first activation", value=(con[0,15,15,1]))
        con = self._denoise(con)
        self._logger._log_noise_budget(message="Noise budget after denoise", value=(con[0,15,15,1]))

        # layer 3 - mean pooling
        with TimerContext(headline='meanpooling', message="time taken for first meanpool:"):
            con = self._meanpool2(con)
    
        # self._logger._log_message('con1mean shape: %s', str(con.shape))
        self._logger._log_noise_budget(message="Noise budget after first meanpool", value=(con[0,8,8,1]))

        # layer 4 - convolution, bias addition 
        with TimerContext(headling='Second convolution', message="time taken for second convolution:"):
            con = self._conv2d(con,self._model['wc2'])
            con = self._add_bias(con,self._model['bc2'])
        # self._logger._log_message('con2b shape: %s', str(con.shape))
        
        self._logger._log_noise_budget(message="Noise budget after second convolution", value=(con[0,8,8,1]))
        con = self._denoise(con)
        self._logger._log_noise_budget(message="Noise budget after denoise", value=(con[0,8,8,1]))


        # layer 5 - mean pooling 
        with TimerContext(headling='meanpooling', message="time taken for second meanpooling:"):
            con = self._meanpool2(con)
        # self._logger._log_message('mean2 shape: %s', str(con.shape))

        self._logger._log_noise_budget(message="Noise budget after second meanpool", value=(con[0,5,5,1]))

        # layer 6 - fully connected 
        newshape = con.shape[1]*con.shape[2]*con.shape[3]
        fc = np.reshape(con,(newshape))

        with TimerContext(headling="Fully connected layer 1", message="time taken for first fully connected layer:"):
            fc = self._fully_connect(fc,self._model['wf1'])

        self._logger._log_noise_budget(message="Noise budget after first fcl", value=(fc[99]))
        fc = self._denoise(fc)
        self._logger._log_noise_budget(message="Noise budget after denoise", value=(fc[99]))
        fc = self._add_bias1(fc,self._model['bf1'])

        # layer 7 - activation
        with TimerContext(headling='activation', message="time taken for activation 2:"):
            fc = self._activ_fun(fc)
        
        self._logger._log_noise_budget(message="Noise budget after first fcl act", value=(fc[99]))
        fc = self._denoise(fc)
        self._logger._log_noise_budget(message="Noise budget after denoise", value=(fc[99]))

        # layer 8 - fully connected
        with TimerContext(headline='Fully connected layer 2', message="time taken for second fcl:"):
            fc2 = self._fully_connect(fc,self._model['otw'])

        self._logger._log_noise_budget(message="Noise budget second fcl", value=(fc2[4]))
        fc = self._denoise(fc)
        self._logger._log_noise_budget(message="Noise budget after denois", value=(fc2[4]))
        logits = self._add_bias1(fc2,self._model['otb'])

        return logits


    def _dot_product(self,enc_x, plain_b):
        
        dt = np.dtype('O')
        dotProduct = np.zeros((len(enc_x), len(plain_b[0])), dtype = dt)

        if len(enc_x[0,:]) != len(plain_b[:,0]):
            self._logger._log_message("dimension mismatch for dot product")
            return 0    
        
        for j in range(len(dotProduct[:,0])):
            self._logger._log_message("%d / %d",j,len(dotProduct[:,0]))

            for i in range(len(dotProduct[0, :])):
                sumt = self._Ciphertext()
                self._encryptor.encrypt(self._encoder.encode(0),sumt)
                
                for column in range(len(enc_x[0, :])):
                   
                    temp_x = self._Ciphertext(enc_x[j,column])
                    self._evaluator.multiply_plain(temp_x, self._encoder.encode(plain_b[column,i]))
                    
                    self._evaluator.add(sumt,temp_x)
                    
                dotProduct[j, i] = sumt
                # self._logger._log_message("idx: %d,%d: %s",j,i,str(self._debughandler.decrypt_ciphertext(dotProduct[j,i])))
        
        return dotProduct


    def _calc_pad(self, pad, in_siz, out_siz, stride, ksize):
        """Calculate padding width.

        Args:
            pad: padding method
            ksize: kernel size [I, J].

        Returns:
            pad_: Actual padding width.
        """
        _mapping = {
            'SAME': (out_siz - 1) * stride + ksize - in_siz,
            'VALID': 0
        }
        return pad if pad not in _mapping else _mapping.get(pad)

    def _calc_size(self, h, kh, pad, sh):
        """Calculate output image size on one dimension.

        Args:
            h: input image size.
            kh: kernel size.
            pad: padding strategy.
            sh: stride.

        Returns:
            s: output size.
        """

        _mapping = {
            'SAME': np.ceil(h / sh),
            'VALID': np.ceil((h - kh + 1) / sh)
        }
        
        return int(np.ceil((h - kh + pad + 1) / sh)) if pad not in _mapping else _mapping.get(pad)

    def _extract_sliding_windows(self,x, ksize, pad, stride, floor_first=True):
        """Converts a matrix to sliding windows.

        Args:
            x: [N, H, W, C]
            k: [KH, KW]
            pad: [PH, PW]
            stride: [SH, SW]

        Returns:
            y: [N, (H-KH+PH+1)/SH, (W-KW+PW+1)/SW, KH * KW, C]
        """
        n, h, w, c = x.shape
        kh, kw = ksize
        sh, sw = stride

        h2, w2 = int(self._calc_size(h, kh, pad, sh)), int(self._calc_size(w, kw, pad, sw))
        ph, pw = int(self._calc_pad(pad, h, h2, sh, kh)), int(self._calc_pad(pad, w, w2, sw, kw))

        ph0, ph1 = int(np.floor(ph / 2)), int(np.ceil(ph / 2))
        pw0, pw1 = int(np.floor(pw / 2)), int(np.ceil(pw / 2))

        pph = (ph1, ph0)
        ppw = (pw1, pw0)

        if floor_first:
            pph = (ph0, ph1)
            ppw = (pw0, pw1)

        x = np.pad(
            x,
            ((0, 0), pph, ppw, (0, 0)),
            mode='constant',
            constant_values=(0.0,)
        )
        dt = np.dtype('O')
        y = np.zeros([n, h2, w2, kh, kw, c], dtype = dt)
        #y = np.zeros([n, h2, w2, kh, kw, c])
        for ii in range(h2):
            for jj in range(w2):
                xx = ii * sh
                yy = jj * sw
                y[:, ii, jj, :, :, :] = x[:, xx:xx + kh, yy:yy + kw, :]

        return y

    def _conv2d(self,x, w, pad='SAME', stride=(1, 1)):
        """2D convolution 

        Args:
            x: [N, H, W, C]
            w: [I, J, C, K]
            pad: [PH, PW]
            stride: [SH, SW]

        Returns:
            y: [N, H', W', K]
        """

        ksize = w.shape[:2]
        x = self._extract_sliding_windows(x, ksize, pad, stride)
        ws = w.shape
        w = w.reshape([ws[0] * ws[1] * ws[2], ws[3]])
        xs = x.shape
        x = x.reshape([xs[0] * xs[1] * xs[2], -1])
       
        # the matrix was padded with zeros. so we replace those zeros with encrypted value of 0,
        # in order to maintain uniformity of object types in the matrix 
        enc_0 = self._Ciphertext()
        self._encryptor.encrypt(self._encoder.encode(0),enc_0)

        for i in range(x.shape[0]):
            for j in range(x.shape[1]):
                if x[i,j] == 0:
                    x[i,j] = enc_0

        y = self._dot_product(x,w)
        y = y.reshape([xs[0], xs[1], xs[2], -1])
        return y


    def _add_bias(self,a,b):
        """Bias addition after convolution -  bias to be added to the entire matrix in each channel
    
        Args:
            a: [1,height,width,channels] 
            b: [channels]

        Returns:
            a: [1,height,width,channels]
        """
        
        for i in range(a.shape[3]):
            for j in range(a.shape[2]):
                for k in range(a.shape[1]):
                    self._evaluator.add_plain(a[0,j,k,i],self._encoder.encode(b[i]))
                    
        return a

    def _add_bias1(self,x,y):
        """bias addition after fc - only one channel but different bias added to each node(element in the array)

        Args:
            x: [#nodes]
            y: [#nodes]

        Returns:
            x: [#nodes]
        """
        
        for i in range(len(x)):
            self._evaluator.add_plain(x[i],self._encoder.encode(y[i]))
            
        return x

    def _activ_fun(self,x):
        """activation function - applies the activation (square) function to every element in the input array/ matrix

        Args:
            x

        Returns:
            x
        """
        if len(x.shape) == 0:
            return x

        if len(x.shape) == 1:
            print('x',x)
            s1 = x.shape[0]
            print('s1',s1)
            # squared = np.zeros((s1))
            for i in range(s1):
                # self._evaluator.square(x[i])
                self._approximation_func(encrypted_x=x[i])
                print(_approx)
                self._evaluator.relinearize(x[i],self._ev_key) 
        else:
            __, s1, s2, s3 = x.shape
            
            for i in range(s1):
                for j in range(s2):
                    for k in range(s3):
                        # self._evaluator.square(x[0,i,j,k])
                        self._approximation_func(encrypted_x=x[0, i, j, k])
                        # print(_approx )
                        self._evaluator.relinearize(x[0,i,j,k],self._ev_key)
        return x


    def _meanpool2(self,x):
        """meanpool2 takes (1,height,width,channels) input and performs meanpooling on each of the #channels
           matrices seperately and gives a (1,height/2,width/2,channels) output

        Args:
            x: [1,n,h,c]

        Returns:
            y: [1,n/2,h/2,c]
        """
        dt = np.dtype('O')
        retval = np.zeros((1,int(x.shape[1]/2),int(x.shape[2]/2),x.shape[3]),dtype = dt)
        for chan in range(x.shape[3]):
            ii,jj,i,j=0,0,0,0
            while i < x.shape[1]:
                j,jj=0,0
                while j < x.shape[2]:
                    res = self._Ciphertext()
                    advals = [x[0,i,j,chan],x[0,i+1,j,chan],x[0,i,j+1,chan],x[0,i+1,j+1,chan]]
                    self._evaluator.add_many(advals,res)
                    self._evaluator.multiply_plain(res,self._encoder.encode(0.25))
                    retval[0,ii,jj,chan] = res
                    jj+=1
                    j+=2
                ii+=1
                i+=2

        return retval

        """fully_connect takes an array of length n input and multiplies with an (n x m) matrix to give an array of length m output

        Args:
            x: [n]
            y: [n,m]

        Returns:
            z: [m]
        """
    def _fully_connect(self,x,y):
        retval = np.zeros((y.shape[1]),dtype=np.dtype('O'))

        for i in range(y.shape[1]):
            res = self._Ciphertext()
            self._encryptor.encrypt(self._encoder.encode(0),res)
            for j in range(y.shape[0]):
                temp_x = self._Ciphertext(x[j])
                self._evaluator.multiply_plain(temp_x, self._encoder.encode(y[j,i]))
                self._evaluator.add(res,temp_x)
            retval[i] = res
        return retval

    def _approximation_func(self, encrypted_x):
        a = -4.44089209850063e-18 * int(''.join(map(str, x[i])))
        a = a ** 3

        b = 0.038268343236509 * int(''.join(map(str, x[i])))
        b = b ** 2

        c = 0.5 * int(''.join(map(str, x[i])))
        d = 1.35299025036549
        result =  a + b + c + d
        
