import seal
import numpy as np
from seal import ChooserEvaluator,     \
                 Ciphertext,           \
                 Decryptor,            \
                 Encryptor,            \
                 EncryptionParameters, \
                 Evaluator,            \
                 IntegerEncoder,       \
                 FractionalEncoder,    \
                 KeyGenerator,         \
                 MemoryPoolHandle,     \
                 Plaintext,            \
                 SEALContext,          \
                 EvaluationKeys,       \
                 GaloisKeys,           \
                 PolyCRTBuilder,       \
                 ChooserEncoder,       \
                 ChooserEvaluator,     \
                 ChooserPoly
                 
class EncryptionHandler:
    config = {
        'poly_modulus':'1x^1024 + 1',
        'coeff_modulus':1024,
        'int_coeff':64,
        'fract_coeff':32,
        'base':2
    }

    def __init__(self):
        self.Ciphertext = Ciphertext
        self.Plaintext = Plaintext()
        self._context = self._build_context()
        self._setup_members(self._context)
        

    def _build_context(self):

        #set up encryption parameters and context
        parms = EncryptionParameters()
        parms.set_poly_modulus(self.config['poly_modulus'])
        parms.set_coeff_modulus(seal.coeff_modulus_128(self.config['coeff_modulus']))
        parms.set_plain_modulus(1 << 18)
        context = SEALContext(parms)

        return context

    def _create_keys(self, context):

        keygen = KeyGenerator(context)
        public_key = keygen.public_key()
        secret_key = keygen.secret_key()

        ev_keys16 = EvaluationKeys()
        keygen.generate_evaluation_keys(16, ev_keys16)

        return public_key, secret_key, ev_keys16

    def _setup_members(self, context):

        self._encoder = FractionalEncoder(
            context.plain_modulus(),
            context.poly_modulus(),
            self.config['int_coeff'],
            self.config['fract_coeff'],
            self.config['base']
        )
        self._evaluator = Evaluator(context)

        pk, sk, self._ev_key = self._create_keys(context)
        self._encryptor = Encryptor(context, pk)
        self._decryptor = Decryptor(context, sk)

    def get_matrix(self, mat):
        shape = mat.shape
        assert(len(shape) == 1 or len(shape) == 4)

        if len(shape)==1:
            p_c = np.zeros((shape[0]))
            for i in range(shape[0]):
                p_c[i] = self.decrypt_ciphertext(mat[i])
        else:
            p_c = np.zeros((shape[0],shape[1],shape[2],shape[3]))
            for i in range(shape[1]):
                for j in range(shape[2]):
                    for k in range(shape[3]):
                        p_c[0,i,j,k] = self.decrypt_ciphertext(mat[0,i,j,k])
            
        return p_c

    def decrypt_ciphertext(self, cipher):
        plain = Plaintext()
        self.decryptor.decrypt(cipher, plain)
        pl =  self.encoder.decode(plain)
        
        return pl

    def re_encrypt(self, x):
        shape = x.shape
        prod = np.prod(shape)
        x = np.reshape(x,prod)

        temp_x = self.get_matrix(x)

        x = []
        for i in range(prod):
            x.append(Ciphertext())
            self.encryptor.encrypt(self.encoder.encode(temp_x[i]), x[i])

        return np.reshape(x,shape)

    @property
    def encryptor(self):
        return self._encryptor

    @property
    def evaluator(self):
        return self._evaluator

    @property
    def encoder(self):
        return self._encoder

    @property
    def decryptor(self):
        return self._decryptor
    
    @property
    def evaluation_key(self):
        return self._ev_key
