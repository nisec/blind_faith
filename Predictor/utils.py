import time


class TimerContext:
    def __init__(self, message, headline=""):
        self.start = 0
        self.message = message
        self.headline = headline

    def __enter__(self):
        print(self.headline)
        self.start = time.clock()
        return self

    def __exit__(self, *exc_info):
        print("{}:  {}s".format(self.message, time.clock() - self.start))