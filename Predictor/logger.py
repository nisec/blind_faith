import logging



class Logger:
    def __init__(self, debug, handler=None):
        logging.basicConfig(format='\n[%(levelname)s]: %(asctime)s {%(name)s} \n\t\t-- %(message)s\n',datefmt='%I:%M:%S %p')

        self._logger = logging.getLogger(__name__)
        self._logger.setLevel(logging.INFO)

        self.handler = handler
        self._debug = debug
        if debug:
            self._logger.setLevel(logging.DEBUG)


    def _log_message(self, message):
        if not self._debug:
            return
        
        self._logger.debug(message)
    
    def _log_noise_budget(self, message, value):
        self._log_message(message="{}: {} bits".format(message, (str)(self.handler.decryptor.invariant_noise_budget(value))))