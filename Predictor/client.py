from Predictor.logger import Logger
from encryption_handler.encryption_handler import EncryptionHandler
from server_net.predictor_net import Predictor
# import PyQt4
# import matplotlib
# matplotlib.use('Agg')
#import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import argparse
from Predictor.utils import TimerContext

#print(matplotlib.get_backend())

parser = argparse.ArgumentParser()
parser.add_argument('filename', type=str) 
parser.add_argument('debug', type=bool) 

def main():
    options = parser.parse_args()

    debug = options.debug
    test_im = np.load("Predictor/images/{}.npy".format(options.filename))
    ln = test_im.shape[0]

    plt.imshow(np.reshape(test_im,(28,28)))
    # plt.plot([1,2,2,3,4,5])
    plt.show()
    # plt.savefig("newfigtest")
    # raw_input()
    handler = EncryptionHandler()
    logger = Logger(debug=debug, handler=handler)

    #encrypt image
    with TimerContext(message='total time taken for the network:'):

        with TimerContext(message='time taken for encrypting image:'):
            encrypted_image = []
            for i in range(ln):
                encrypted_image.append((handler.Ciphertext()))
                handler.encryptor.encrypt(handler.encoder.encode(test_im[i]), encrypted_image[i])

        logger._log_noise_budget(message="Noise budget in fresh encryption", value=encrypted_image[100])
        #call the Fpredictor 
        # predictor =  Predictor(op, debug = True, decryptor = handler.decryptor)
        predictor = Predictor(debug=debug, handler=handler)
        logits = predictor.predict_image(encrypted_image)

        with TimerContext(message='time taken for decrypting image:'):
            dec_logits=(handler.get_matrix(logits))
            logger._log_message(message=dec_logits)
            logger._log_message(message="Prediction: {}".format((str)(np.argmax(dec_logits))))

    # count = 0
    # for i in a:
    #     print(count+1)
    #     print(handler.decrypt_mat(i))
    #     count+=1
if __name__ == '__main__':
    main()
